import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_tracker_app/app/sign_in_page/sign_in.dart';
import 'package:time_tracker_app/pages/home/home_page.dart';
import 'package:time_tracker_app/services/auth.dart';
import 'package:time_tracker_app/services/database.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthBase>(context, listen: false);
    return StreamBuilder<User>(
        stream: auth.authStateChanges(),
        builder: (context, snapshot) {
          final User user = snapshot.data;
          if (snapshot.connectionState == ConnectionState.active) {
            if (user == null) {
              return SignInPage.create(context);
            }
            return Provider(
              create: (_) => FirestoreDatabase(uid: user.uid),
              child: HomePage(),
            );
          }
          return Scaffold(
            body: CircularProgressIndicator(),
          );
        });
  }
}
