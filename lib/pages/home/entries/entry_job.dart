import 'package:time_tracker_app/pages/models/entry.dart';
import 'package:time_tracker_app/pages/models/job.dart';

class EntryJob {
  EntryJob(this.entry, this.job);

  final Entry entry;
  final Job job;
}
