import 'package:flutter/material.dart';
import 'package:time_tracker_app/pages/home/account/account_page.dart';
import 'package:time_tracker_app/pages/home/cupertino_home_scaffold.dart';
import 'package:time_tracker_app/pages/home/entries/entries_page.dart';
import 'package:time_tracker_app/pages/home/job_entries/entry_page.dart';
import 'package:time_tracker_app/pages/home/job_page.dart';
import 'package:time_tracker_app/pages/home/tab_item.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TabItem _currentTab = TabItem.jobs;

  void _select(TabItem tabItem) {
    if (tabItem == _currentTab) {
      // pop to first route
      navigatorKeys[tabItem].currentState.popUntil((route) => route.isFirst);
    } else {
      setState(
        () => _currentTab = tabItem,
      );
    }
  }

  Map<TabItem, GlobalKey<NavigatorState>> navigatorKeys = {
    TabItem.jobs: GlobalKey<NavigatorState>(),
    TabItem.entries: GlobalKey<NavigatorState>(),
    TabItem.account: GlobalKey<NavigatorState>(),
  };

  Map<TabItem, WidgetBuilder> get widgetBuilders {
    return {
      TabItem.jobs: (_) => JobPage(),
      TabItem.entries: (context) => EntriesPage.create(context),
      TabItem.account: (_) => AccountPage(),
    };
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async =>
          !await navigatorKeys[_currentTab].currentState.maybePop(),
      child: CupertinoHomeScaffold(
        navigatorKeys: navigatorKeys,
        currentTab: _currentTab,
        onSelected: _select,
        widgetBuilders: widgetBuilders,
      ),
    );
  }
}
