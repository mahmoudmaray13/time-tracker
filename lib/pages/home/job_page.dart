import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_tracker_app/common_widget/show_alert_dialog.dart';
import 'package:time_tracker_app/common_widget/show_exception_alert_dialog.dart';
import 'package:time_tracker_app/pages/home/edit__job_page.dart';
import 'package:time_tracker_app/pages/home/job_entries/job_entries_page.dart';
import 'package:time_tracker_app/pages/home/job_list_tile.dart';
import 'package:time_tracker_app/pages/home/list_item_builder.dart';
import 'package:time_tracker_app/pages/models/job.dart';
import 'package:time_tracker_app/services/auth.dart';
import 'package:time_tracker_app/services/database.dart';

class JobPage extends StatelessWidget {


  Future<void> _createJob(BuildContext context) async {
    try {
      final database = Provider.of<FirestoreDatabase>(
        context,
        listen: false,
      );
      await database.setJob(Job(name: 'Blogging', ratePerHour: 10));
    } on FirebaseException catch (e) {
      showExceptionAlertDialog(
        context,
        title: 'Operation failed',
        exception: e,
      );
    }
  }

  Future<void> _delete(BuildContext context, Job job) async {
    try {
      final database = Provider.of<FirestoreDatabase>(context, listen: false);
      await database.deleteJob(job);
    } on FirebaseException catch (e) {
      showExceptionAlertDialog(
        context,
        title: 'Operation failed',
        exception: e,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Jobs'),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.add, color: Colors.white),
            onPressed:() => EditJobPage.show(
              context,
              database: Provider.of<FirestoreDatabase>(context, listen: false),
            ),
          ),
        ],
      ),

      body: _buildContents(context),
    );
  }

  Widget _buildContents(BuildContext context) {
    final database = Provider.of<FirestoreDatabase>(context, listen: false);
    return StreamBuilder<List<Job>>(
      stream: database.jobsStream(),
      builder: (context, snapshot) {
        return ListItemsBuilder<Job>(
          snapshot: snapshot,
          itemBuilder: (context, job) => Dismissible(
            key: Key('job-${job.id}'),
            background: Container(
              color: Colors.red,
            ),
            direction: DismissDirection.endToStart,
            onDismissed: (direction) => _delete(context, job),
            child: JobListTile(
              job: job,
              onTap: () => JobEntriesPage.show(
                context,
                job,
              ),
            ),
          ),
        );
      },
    );
  }
}
