import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:time_tracker_app/app/sign_in_page/email_sign_in_form_stateful.dart';
import 'package:time_tracker_app/services/auth.dart';

import 'mocks.dart';

void main() {
  MockAuth mockAuth;
  setUp(() {
    mockAuth = MockAuth();
  });

  Future<void> pumpEmailSignInForm(WidgetTester tester,
      {VoidCallback onSignedIn}) async {
    await tester.pumpWidget(
      Provider<AuthBase>(
        create: (_) => mockAuth,
        child: MaterialApp(
          home: Scaffold(
            body: EmailSignInFormStateful(
              onSignedIn: onSignedIn,
            ),
          ),
        ),
      ),
    );
  }

  void stubSignInWithEmailAndPasswordSucceeds() {
    when(mockAuth.signInWithEmailAndPassword(any, any))
        .thenAnswer((_) => Future<User>.value(MockUser()));
  }
  void stubSignInWithEmailAndPasswordThrows(){
    when(mockAuth.signInWithEmailAndPassword(any, any))
        .thenThrow(FirebaseAuthException(code: 'Error_Wrong_Password'));
  }

  group('signIn', () {
    testWidgets(
        'WHEN user does\'t enter the email and password'
        'AND user taps on sign-in button'
        'THEN signInWithEmailAndPassword is not call'
        'AND user is not sign in', (WidgetTester tester) async {
      var signedIn = false;
      await pumpEmailSignInForm(tester, onSignedIn: () => signedIn = true);

      final singInButton = find.text('Sign in');
      await tester.tap(singInButton);
      verifyNever(mockAuth.signInWithEmailAndPassword(any, any));
      expect(signedIn, false);
    });

    testWidgets(
        'WHEN user  enters a valid email and password'
        'AND user taps on sign-in button'
        'THEN signInWithEmailAndPassword is  called'
        'AND user sign in', (WidgetTester tester) async {
      var signedIn = false;
      await pumpEmailSignInForm(tester, onSignedIn: () => signedIn = true);

      stubSignInWithEmailAndPasswordSucceeds();

      const email = 'email@example.com';
      const password = 'password';

      final emailField = find.byKey(Key('email'));
      expect(emailField, findsOneWidget);
      await tester.enterText(emailField, email);

      final passwordField = find.byKey(Key('password'));
      expect(passwordField, findsOneWidget);
      await tester.enterText(passwordField, password);

      await tester.pump();

      final singInButton = find.text('Sign in');
      await tester.tap(singInButton);

      verify(mockAuth.signInWithEmailAndPassword(any, any));
      expect(signedIn, true);
    });
    testWidgets(
        'WHEN user  enters an in valid email and password'
            'AND user taps on sign-in button'
            'THEN signInWithEmailAndPassword is  called'
            'AND user not sign in', (WidgetTester tester) async {
      var signedIn = false;
      await pumpEmailSignInForm(tester, onSignedIn: () => signedIn = true);

      stubSignInWithEmailAndPasswordThrows();

      const email = 'email@example.com';
      const password = 'password';

      final emailField = find.byKey(Key('email'));
      expect(emailField, findsOneWidget);
      await tester.enterText(emailField, email);

      final passwordField = find.byKey(Key('password'));
      expect(passwordField, findsOneWidget);
      await tester.enterText(passwordField, password);

      await tester.pump();

      final singInButton = find.text('Sign in');
      await tester.tap(singInButton);

      verify(mockAuth.signInWithEmailAndPassword(email, password));
      expect(signedIn, false);
    });
  });

  testWidgets(
      'WHEN user taps on secondary button'
      'THEN from toggles to registration mode', (WidgetTester tester) async {
    await pumpEmailSignInForm(tester);
    final registerButton = find.text('Need an account ? Register');
    await tester.tap(registerButton);
    await tester.pump();
    final createAccountButton = find.text('Create an account');
    expect(createAccountButton, findsOneWidget);
  });

  testWidgets(
      'WHEN user taps on secondary button'
      'AND user  enters the email and password'
      'AND user taps on register button'
      'THEN signInWithEmailAndPassword is  called',
      (WidgetTester tester) async {
    await pumpEmailSignInForm(tester);

    const email = 'email@example.com';
    const password = 'password';

    final registerButton = find.text('Need an account ? Register');
    await tester.tap(registerButton);

    await tester.pump();

    final emailField = find.byKey(Key('email'));
    expect(emailField, findsOneWidget);
    await tester.enterText(emailField, email);

    final passwordField = find.byKey(Key('password'));
    expect(passwordField, findsOneWidget);
    await tester.enterText(passwordField, password);

    await tester.pump();

    final createAccountButton = find.text('Create an account');
    expect(createAccountButton, findsOneWidget);
    await tester.tap(createAccountButton);

    verify(mockAuth.createUserWithEmailAndPassword(any, any));
  });
}
