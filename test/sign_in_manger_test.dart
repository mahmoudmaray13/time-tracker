import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:time_tracker_app/app/sign_in_page/sign_in_manger.dart';

import 'mocks.dart';

class MockValueNotifier<T> extends ValueNotifier<T> {
  MockValueNotifier(T value) : super(value);

  List<T> values = [];

  set value(T newValue) {
    values.add(newValue);
    super.value = newValue;
  }
}

void main() {
  MockAuth mockAuth;
  MockValueNotifier<bool> isLoading;
  SignInManger manger;

  setUp(() {
    mockAuth = MockAuth();
    isLoading = MockValueNotifier<bool>(false);
    manger = SignInManger(auth: mockAuth, isLoading: isLoading);
  });

  test('sign-in-success', () async {
    when(mockAuth.signInAnonymously())
        .thenAnswer((_) => Future.value(MockUser.uid('123')));
    await manger.signInAnonymously();

    expect(isLoading.values, [true]);
  });
  test('sign-in-failure', ()async {
    when(mockAuth.signInAnonymously())
        .thenThrow(PlatformException(code: 'ERROR', message: 'Sign In failed'));
    try{
      await manger.signInAnonymously();
    } catch (e) {
      expect(isLoading.values, [true,false]);
    }
  });
}
