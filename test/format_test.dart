import 'package:flutter_test/flutter_test.dart';
import 'package:intl/intl.dart';
import 'package:time_tracker_app/pages/home/job_entries/format.dart';
import 'package:intl/date_symbol_data_local.dart';

void main() {
  group('hours', ()
  {
    test('positive', () {
      expect(Format.hours(10), '10h');
    });
    test('zero', () {
      expect(Format.hours(0), '0h');
    });
    test('negative', () {
      expect(Format.hours(-5), '0h');
    });
    test('decimal', () {
      expect(Format.hours(4.5), '4.5h');
    });
    group('data- GB local', () {
      setUp(() async {
        Intl.defaultLocale = 'en_GB';
        await initializeDateFormatting(Intl.defaultLocale);
      });
      test('2021-04-24', () {
        expect(
          Format.date(DateTime(2021, 4, 24)),
          '24 Apr 2021',
        );
      });
    });
    group('dayOfWeek-GB local', () {
      setUp(() async {
        Intl.defaultLocale = 'en-GB';
        await initializeDateFormatting(Intl.defaultLocale);
      });
      test('Saturday', () {
        expect(
          Format.dayOfWeek(DateTime(2021, 4, 24)),
          'Sat',
        );
      });
    });
    group('Currency-US local', () {
      setUp(() async {
        Intl.defaultLocale = 'en-Us';
        await initializeDateFormatting(Intl.defaultLocale);
      });
      test('positive', () {
        expect(Format.currency(10), '\$10',);
      });
      test('zero', () {
        expect(Format.currency(0), '',);
      });
      test('negative', () {
        expect(Format.currency(-5), '-\$5');
      });
    });
  }
);
}